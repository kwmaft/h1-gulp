# Hausübung 1   

Als Vorbeitung für die nächste Einheit, müssen alle notwendigen Tools auf euren privaten Laptops installiert sein. Dazu zählen

- Node.js und NPM
- Gulp.js
- Webpack

## Node.js und NPM installieren

Um Node.js und NPM zu installieren, muss der Installer für das jeweilige System (Windows, Mac, Linux) heruntergeladen werden.
Empfohle wird die aktuelle LTS Version (8.10.0) zu installieren.

- https://nodejs.org/en/download/

Für die Installation von Node.js müssen keine besonderen Einstellungen getroffen werden. Beim Installer einfach immer auf weiter klicken.

Nachdem Node.js installiert wurde, könnt ihr mit folgenden Befehlen in der Konsole testen, ob die Installation erfolgreich war.

- ``node -v`` sollte dann ``v8.10.0`` ausgeben
- ``npm -v`` sollte dann ``5.6.0`` ausgeben

## Folgende Beispiele

In den weiteren Unterpunkten werden immer wieder Konsolenbefehle aufgelistet. Nachdem ihr dieses Repository mit PHPStorm ausgecheckt habt,
könnt ihr die Konsole von PHPStorm direkt verwenden, was die Prozesse etwas vereinfacht. Das Repository könnt ihr direkt mit PHPStorm auschecken,
dazu einfach ``VCS > Checkout from Version Control > Git`` in PHPStorm wählen und den Link des Repositories in die erste Zeile kopieren.

- https://bitbucket.org/kwmaft/h1-gulp.git

![Git Beispiel](./img/git.png)

## Gulp.js installieren

Um Gulp.js verwenden zu können, muss Gulp.js zuerst mit NPM global installiert werden. Das ist mit folgendem Befehl möglich.

- ``npm install -g gulp``

Danach kann der Befehl ``gulp`` in der Konsole verwendet werden. Damit Gulp.js aber für ein Projekt verwendet werden kann,
sind zwei Dinge notwendig. Eine dazugehörige Datei ``gulpfile.js`` in dem Verzeichnis, in dem Gulp.js verwendet werden soll.
Als zweites wird Gulp.js als lokale Dependency benötigt. Dafür muss in dem Verzeichnis, in dem das ``gulpfile.js`` liegt folgender
Befehl ausgeführt werden.

- ``npm install gulp`` (nicht sofort ausführen)

In diesem Repository ist eine ``package.json`` Datei vorhanden, mit der alle notwendigen Packages installiert werden können.
Einfach in der Konsole von PHPStorm ``npm install`` ausführen. Im Prinzip installiert dieser Befehl lediglich Gulp.js als NPM Modul.

Sollte npm wegen Vulnerabilities aufschreien, könnt ihr einfach nach ``npm install`` den Befehl ``npm audit fix`` ausführen.

Jetzt könnt ihr Gulp.js ausführen, um zu prüfen ob die Installation erfolgreich war. Einfach in der Konsole folgenden Befehl ausführen.

- ``gulp``

Dann sollte in der Konsole angezeigt werden, welches gulpfile verwendet wird und folgende Zeilen ausgegeben werden.

- Starting 'default' ...
- Gulp.js works!
- Finished 'default' after ...

Was diese Zeilen bedeuten und wie alles funktioniert, schauen wir uns in der nächsten Einheit an.

## Webpack installieren

Webpack muss nicht seperat installiert werden, da wir Webpack direkt mit Gulp.js verwenden werden. Wurde bei ``npm install`` automatisch
mitinstalliert.